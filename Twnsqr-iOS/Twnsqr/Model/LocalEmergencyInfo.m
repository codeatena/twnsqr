//
//  LocalEmergencyInfo.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "LocalEmergencyInfo.h"

@implementation LocalEmergencyInfo

@synthesize title, arrSubLocalEmergencyInfo;

- (id) init
{
    self = [super init];
    
    title = @"";
    arrSubLocalEmergencyInfo = [[NSMutableArray alloc] init];
    
    return self;
}

- (id) initWithTitle:(NSString*) _title
{
    self = [self init];
    self.title = _title;
    
    return self;
}

@end