//
//  Section.h
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Section : NSObject

@property (nonatomic, retain) NSString* header;
@property (nonatomic, retain) NSMutableArray* arrPostData;

@end
