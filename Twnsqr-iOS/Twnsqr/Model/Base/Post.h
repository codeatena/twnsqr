//
//  Post.h
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSMutableArray* arrSection;
@property (nonatomic, retain) NSString* header1;
@property (nonatomic, retain) NSString* header2;
@property (nonatomic, retain) NSDate*   date;

@end