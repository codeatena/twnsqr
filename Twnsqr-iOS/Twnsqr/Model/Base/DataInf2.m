//
//  DataInf2.m
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DataInf2.h"

@implementation DataInf2

@synthesize title, header1, header2;

- (id) init
{
    self = [super init];
    if (self) {
        title = @"3:00pm-5:00pm  ArtShow at The ArtGallery";
        header1 = @"Blennerhasett Island";
        header2 = @"0.05 Miles Away";
    }
    
    return self;
}

@end
