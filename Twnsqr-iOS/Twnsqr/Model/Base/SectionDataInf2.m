//
//  SectionDataInf2.m
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "SectionDataInf2.h"
#import "DataInf2.h"

@implementation SectionDataInf2

@synthesize header1, header2, arrDataInf2;

- (id) init
{
    self = [super init];
    if (self) {
        header1 = @"Right Now 4:15PM";
        header2 = @"";
        arrDataInf2 = [[NSMutableArray alloc] init];
        
        DataInf2* dataInf2 = [[DataInf2 alloc] init];
        [arrDataInf2 addObject:dataInf2];
        
        dataInf2 = [[DataInf2 alloc] init];
        dataInf2.title = @"12:00pm - 6:00pm  Super Sale at Point Park";
        [arrDataInf2 addObject:dataInf2];
    }
    
    return self;
}

@end
