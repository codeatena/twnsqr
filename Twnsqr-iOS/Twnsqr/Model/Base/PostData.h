//
//  PostData.h
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostData : NSObject

@property (nonatomic, retain)   NSString*   content;
@property (nonatomic, retain)   UIColor*    color;
@property (nonatomic)           int         nType;

@end
