//
//  DataInf1.m
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DataInf1.h"

@implementation DataInf1

@synthesize header1, header2, title, desc;

- (id) init
{
    self = [super init];
    if (self) {
        header1 = @"City of Parkersburg";
        header2 = @"Mon, Sep 15";
        title = @"ParkersBurg Farmers MarketPlace Downtown Parkersburg";
        desc = @"Don't miss the Farmers MarketPlace Downtown Friday at the Bicentennial Plaza 302 Market St Parkersburg, WV http://www.parkersbufgfarmersmarket.com";
    }
    
    return self;
}

@end
