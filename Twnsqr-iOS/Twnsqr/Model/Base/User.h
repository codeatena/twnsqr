//
//  User.h
//  Twnsqr
//
//  Created by Hang Chung on 6/20/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, retain) NSString* userID;
@property (nonatomic, retain) NSString* userName;
@property (nonatomic, retain) NSString* mail;
@property (nonatomic, retain) NSString* sessionID;
@property (nonatomic, retain) NSString* sessionName;

@end
