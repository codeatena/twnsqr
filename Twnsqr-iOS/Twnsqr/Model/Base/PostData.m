//
//  PostData.m
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "PostData.h"
#import "define.h"

@implementation PostData

@synthesize nType, content, color;

- (id) init
{
    self = [super init];
    if (self) {
        content = @"";
        color = [UIColor blackColor];
        nType = DEFAULT_TYPE;
    }
    return self;
}

@end
