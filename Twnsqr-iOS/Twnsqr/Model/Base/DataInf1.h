//
//  DataInf1.h
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataInf.h"

@interface DataInf1 : DataInf

@property (nonatomic, retain) NSString* desc;

@end
