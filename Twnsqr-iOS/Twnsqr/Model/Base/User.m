//
//  User.m
//  Twnsqr
//
//  Created by Hang Chung on 6/20/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize userID, userName, mail, sessionID, sessionName;

- (id) init
{
    self = [super init];
    
    if (self) {
        userID = @"";
        userName = @"";
        mail = @"";
        sessionID = @"";
        sessionName = @"";
    }

    return self;
}
@end
