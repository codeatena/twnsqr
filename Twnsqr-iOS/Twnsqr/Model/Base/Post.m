//
//  Post.m
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "Post.h"

@implementation Post

@synthesize title, arrSection, header1, header2, date;

- (id) init
{
    self = [super init];
    if (self) {
        title = @"";
        arrSection = [[NSMutableArray alloc] init];
        header1 = @"";
        header2 = @"";
        date = nil;
    }
    return self;
}

@end
