//
//  Section.m
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "Section.h"

@implementation Section

@synthesize header, arrPostData;

- (id) init
{
    self = [super init];
    if (self) {
        header = @"";
        arrPostData = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
