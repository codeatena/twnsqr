//
//  SectionDataInf2.h
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectionDataInf2 : NSObject

@property (nonatomic, retain) NSString* header1;
@property (nonatomic, retain) NSString* header2;
@property (nonatomic, retain) NSMutableArray* arrDataInf2;

@end
