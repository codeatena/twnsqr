//
//  DataInf.m
//  Twnsqr
//
//  Created by Hang Chung on 6/17/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DataInf.h"

@implementation DataInf

@synthesize header1, header2, title, isSaved;

- (id) init
{
    self = [super init];
    if (self) {
        header1 = @"City of Parkersburg";
        header2 = @"Mon, Sep 15";
        title = @"ParkersBurg Farmers MarketPlace Downtown Parkersburg";
        isSaved = NO;
    }
    
    return self;
}

@end
