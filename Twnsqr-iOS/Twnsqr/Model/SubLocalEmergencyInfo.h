//
//  SubLocalEmergencyInfo.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubLocalEmergencyInfo : NSObject

@property (nonatomic, retain) NSString* title; // test commit
@property (nonatomic, retain) NSMutableArray* arrText;

- (id) initWithTitle:(NSString*) _title;

@end