//
//  define.h
//  Twnsqr
//
//  Created by Hang Chung on 6/10/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#ifndef Twnsqr_define_h
#define Twnsqr_define_h

enum BAR_TYPE
{
    ANNOUNCEMENTES = 0,
    ALERTS,
    EMERGENCY,
    DEALS,
    EVENTS,
    ATTRACTIONS,
    JOBS,
};

enum MENU_TYPE
{
    MENU_NONE = 0,
    LOCAL_EMERGENCY_INFO,
    GROUP,
    GROUPS,
    SET_LOCATION,
    FIND_MY_LOCATION,
    EXPAND_YOUR_AREA,
    PROFILE,
    SETTINGS,
    SIGN_IN,
    SIGN_OUT,
    SIGN_UP,
    REPORT_ON_ERROR,
    POST
};

enum POST_DATA_TYPE
{
    DEFAULT_TYPE = 0,
    UNDERLINE_TYPE,
    ITALIC_TYPE,
};

#define NAVIGATION_DURATION             .5f
#define NOTIFICATION_NAME_MENU_POP      @"menu_pop"
#define NOTIFICATION_NAME_MENU_PUSH_SIGN_UP @"menu_push_sign_up"
#define NOTIFICATION_NAME_MENU_PUSH_GROUPS   @"menu_push_groups"
#define NOTIFICATION_NAME_POST          @"post_data"

#define URL_CONTEXT_ROOT            @"http://twnsqr.com/system/apis/"
#define URL_REGISTER                @"http://twnsqr.com/system/apis/twnsqr_user/register"
#define URL_LOGIN                   @"http://twnsqr.com/system/apis/twnsqr_user/login"
#define URL_LOGOUT                  @"http://twnsqr.com/system/apis/twnsqr_user/logout"
#define URL_FIND_LOCATION           @"http://twnsqr.com/system/apis/twnsqr_content/getLocation"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define kOFFSET_FOR_KEYBOARD 120.0f

#endif
