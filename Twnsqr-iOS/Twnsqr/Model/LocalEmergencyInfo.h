//
//  LocalEmergencyInfo.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalEmergencyInfo : NSObject

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSMutableArray* arrSubLocalEmergencyInfo;

- (id) initWithTitle:(NSString*) _title;

@end
