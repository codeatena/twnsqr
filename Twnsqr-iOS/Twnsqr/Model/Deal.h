//
//  Deal.h
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Deal : NSObject

@property (nonatomic, retain) NSString* header;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* location;
@property (nonatomic, retain) NSString* site;
@property (nonatomic, retain) NSDate*   date;

+ (NSString*) getRemainTime:(NSDate*) _date;

@end
