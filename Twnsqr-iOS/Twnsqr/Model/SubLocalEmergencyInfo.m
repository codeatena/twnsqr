//
//  SubLocalEmergencyInfo.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "SubLocalEmergencyInfo.h"

@implementation SubLocalEmergencyInfo

@synthesize title, arrText;

- (id) init
{
    self = [super init];
    
    title = @"";
    arrText = [[NSMutableArray alloc] init];
    
    return self;
}

- (id) initWithTitle:(NSString*) _title
{
    self = [self init];
    self.title = _title;
    
    return self;
}
@end
