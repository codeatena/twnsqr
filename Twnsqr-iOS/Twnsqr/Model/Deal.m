//
//  Deal.m
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "Deal.h"

@implementation Deal

@synthesize header, title, desc, location, site, date;

- (id) init
{
    self = [super init];
    if (self) {
        header = @"Bob's Burgers";
        title = @"$2 Double Cheeseburger DEAL";
        desc = @"For the next 30 Minutes get any double cheesburger for only $2";
        location = @"121 Broad Street Parkersburg WV   304-485-1212";
        site = @"www.bobsbrgerswv.com";
        
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setDay:14];
        [comps setMonth:6];
        [comps setYear:2014];
        [comps setHour:12];
        [comps setMinute:00];
        
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:NSGregorianCalendar];
        date = [NSDate dateWithTimeIntervalSinceNow:1000];
    }
    return self;
}

+ (NSString*) getRemainTime:(NSDate*) _date
{
    //NSLog(@"difference time %f", [date timeIntervalSinceNow]);
    if (_date == nil) return @"";
    
    int d = (int) [_date timeIntervalSinceNow];
    int m = d / 60;
    int s = d % 60;
    
    NSString* str = [NSString stringWithFormat:@"%02d : %02d", m, s];
    
    //NSLog(@"remain time = %@", str);
    return str;
}

@end
