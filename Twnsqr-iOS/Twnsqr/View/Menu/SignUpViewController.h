//
//  SignUpViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController <NSURLConnectionDelegate>
{
    IBOutlet UITextField*   txtUserName;
    IBOutlet UITextField*   txtPassword;
    IBOutlet UITextField*   txtEmail;
    IBOutlet UITextField*   txtRePassword;
    IBOutlet UITextField*   txtDefalutAddress;
    
    NSURLConnection* urlConnection;
    NSMutableData* receivedData;
}

- (IBAction)onOK:(id)sender;
- (IBAction)onCancel:(id)sender;
- (IBAction)onTextInputDone:(id)sender;

- (IBAction)onTextFieldShowKeyboard:(id)sender;
- (IBAction)onTextFieldHideKeyboard:(id)sender;
//- (IBAction)onTouch:(id)sender

@end
