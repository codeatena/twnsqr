//
//  GroupViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupViewController : UIViewController

- (IBAction)onCCMC:(id)sender;
- (IBAction)onParkersburg:(id)sender;
- (IBAction)onWTAP:(id)sender;
- (IBAction)onUnitedBank:(id)sender;
- (IBAction)onTWNSQR:(id)sender;
- (IBAction)onMOVID:(id)sender;

@end
