//
//  GroupViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "GroupViewController.h"
#import "define.h"

@interface GroupViewController ()

@end

@implementation GroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCCMC:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_PUSH_GROUPS object:nil];
}

- (IBAction)onParkersburg:(id)sender
{
    
}

- (IBAction)onWTAP:(id)sender
{
    
}

- (IBAction)onUnitedBank:(id)sender
{
    
}

- (IBAction)onTWNSQR:(id)sender
{
    
}

- (IBAction)onMOVID:(id)sender
{
    
}

@end
