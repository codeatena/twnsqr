//
//  SignInViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "SignInViewController.h"
#import "define.h"
#import "AppDelegate.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [btnSignUp setTitleColor:UIColorFromRGB(0xcb6900) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL) checkTextFieldEmpty: (UITextField*) txtField
{
    NSString* str = txtField.text;
    if ([str isEqualToString:@""] || str == nil) {
        return NO;
    }
    
    return YES;
}

- (BOOL) checkAllTextField
{
    if (![self checkTextFieldEmpty:txtUserName]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please input your username"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    if (![self checkTextFieldEmpty:txtPassword]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please input your password"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)onSignUp:(id)sender
{
    [txtUserName resignFirstResponder];
    [txtPassword resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_PUSH_SIGN_UP object:nil];
}

- (IBAction)onTextInputDone:(id)sender
{
    UITextField* txtField = (UITextField*) sender;
    [txtField resignFirstResponder];
}

- (IBAction)onOK:(id)sender
{
    if ([self checkAllTextField]) {
        [txtUserName resignFirstResponder];
        [txtPassword resignFirstResponder];
        
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:
                                         [NSURL URLWithString: URL_LOGIN]
                                         cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
        NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                        txtUserName.text, @"username",
                                        txtPassword.text, @"password",
                                        nil];
        
        NSError *error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        receivedData = [NSMutableData dataWithCapacity: 0];
        
        // should check for and handle errors here but we aren't
        [urlRequest setHTTPBody:jsonData];
        
        urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
    }
}

- (IBAction)onCancel:(id)sender
{
    [txtUserName resignFirstResponder];
    [txtPassword resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        [receivedData appendData:data];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        // use data from data_users
        NSString *myString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSLog(@"login result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        id jsonData = [jsonObject objectForKey:@"data"];
        NSString* strMsg = [jsonObject objectForKey:@"msg"];
        NSString* strTitle;
        
        if ([strMsg isEqualToString:@""]) {
            strTitle = @"Login Success";
            
            g_delegate.curUser = [[User alloc] init];
            
            g_delegate.curUser.userID = [jsonData valueForKey:@"uid"];
            g_delegate.curUser.userName = [jsonData valueForKey:@"name"];
            g_delegate.curUser.mail = [jsonData valueForKey:@"mail"];
            g_delegate.curUser.sessionID = [jsonData valueForKey:@"sessid"];
            g_delegate.curUser.sessionName = [jsonData valueForKey:@"session_name"];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:g_delegate.curUser.userID forKey:@"user_id"];
            [defaults setObject:g_delegate.curUser.userName forKey:@"user_name"];
            [defaults setObject:g_delegate.curUser.mail forKey:@"user_mail"];
            [defaults setObject:g_delegate.curUser.sessionID forKey:@"session_id"];
            [defaults setObject:g_delegate.curUser.sessionName forKey:@"session_name"];
            
            [defaults synchronize];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:@"You are logined succesfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
        } else if ([strMsg isEqualToString:@"Already logged in as hanhung."]) {
            
            g_delegate.curUser = [[User alloc] init];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            g_delegate.curUser.userID = [defaults objectForKey:@"user_id"];
            g_delegate.curUser.userName = [defaults objectForKey:@"user_name"];
            g_delegate.curUser.mail = [defaults objectForKey:@"user_mail"];
            g_delegate.curUser.sessionID = [defaults objectForKey:@"session_id"];
            g_delegate.curUser.sessionName = [defaults objectForKey:@"session_name"];
            
            NSLog(@"session_id = %@", g_delegate.curUser.sessionID);

        } else {
            strTitle = @"Login Failed";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
