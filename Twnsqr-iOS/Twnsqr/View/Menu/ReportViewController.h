//
//  ReportViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/12/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ReportViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    IBOutlet UITextField*   txtWhichPhone;
    IBOutlet UITextField*   txtWhatProblem;
}

- (IBAction)onOK:(id)sender;
- (IBAction)onCancel:(id)sender;
- (IBAction)onTextInputDone:(id)sender;

@end
