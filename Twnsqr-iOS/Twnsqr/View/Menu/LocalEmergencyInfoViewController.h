//
//  LocalEmergencyInfoViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalEmergencyInfoViewController : UIViewController
{
    NSMutableArray* arrData;
    IBOutlet UIScrollView* scrollView;
}

@end
