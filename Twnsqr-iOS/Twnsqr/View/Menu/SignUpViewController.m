//
//  SignUpViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "SignUpViewController.h"
#import "define.h"
#import "AppDelegate.h"
#import "User.h"


@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL) checkTextFieldEmpty: (UITextField*) txtField
{
    NSString* str = txtField.text;
    if ([str isEqualToString:@""] || str == nil) {
        return NO;
    }
    
    return YES;
}

- (BOOL) checkAllTextField
{
    if (![self checkTextFieldEmpty:txtUserName]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Please input your username"
                                                        delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    if (![self checkTextFieldEmpty:txtEmail]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please input your eamil address"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    if (![self checkTextFieldEmpty:txtPassword]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please input your password"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    
    NSString* strPassword = txtPassword.text;
    NSString* strRePassword = txtRePassword.text;
    
    if (![strPassword isEqualToString:strRePassword]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Your passwords are not matched"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];

        return NO;
    }
    if (![self checkTextFieldEmpty:txtDefalutAddress]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please input your default address"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)onOK:(id)sender
{
    if ([self checkAllTextField]) {
        
        [txtUserName resignFirstResponder];
        [txtEmail resignFirstResponder];
        [txtPassword resignFirstResponder];
        [txtRePassword resignFirstResponder];
        [txtDefalutAddress resignFirstResponder];
        
        NSMutableURLRequest *urlRequest=[NSMutableURLRequest
                                         requestWithURL:[NSURL URLWithString:
                                                         URL_REGISTER]
                                         cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
        NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                        txtUserName.text, @"name",
                                        txtEmail.text, @"mail",
                                        txtPassword.text, @"pass",
                                        @"38.9052", @"lat",
                                        @"121.6064", @"lng",
                                        nil];
        NSError *error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        receivedData = [NSMutableData dataWithCapacity: 0];

        // should check for and handle errors here but we aren't
        [urlRequest setHTTPBody:jsonData];
        
        urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
    }
}

- (IBAction)onCancel:(id)sender
{
    [txtUserName resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtRePassword resignFirstResponder];
    [txtDefalutAddress resignFirstResponder];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
}

- (IBAction)onTextInputDone:(id)sender
{
    UITextField* txtField = (UITextField*) sender;
    [txtField resignFirstResponder];
}

- (IBAction)onTextFieldShowKeyboard:(id)sender
{
//    NSLog(@"show keyboard");
    [self setViewMovedUp:YES];
}

- (IBAction)onTextFieldHideKeyboard:(id)sender
{
//    NSLog(@"hide keyboard");
//    [self setViewMovedUp:NO];
}

-(void)keyboardWillHide {

    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    CGRect rect = self.view.frame;
    
    if (movedUp == YES) {
        if (rect.origin.y < 0) {
            return;
        }
    }  else {
        if (rect.origin.y == 0) {
            return;
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        [receivedData appendData:data];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        // use data from data_users
        NSString *myString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSLog(@"register result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        id jsonData = [jsonObject objectForKey:@"data"];
        NSString* strMsg = [jsonObject objectForKey:@"msg"];
        NSString* strTitle;
        
        if (![strMsg isEqualToString:@"User register success."]) {
            strTitle = @"Register Failed";
        } else {
            strTitle = @"Register Success";
            
//            g_delegate.curUser.userID = [jsonData valueForKey:@"uid"];
//            g_delegate.curUser.userName = [jsonData valueForKey:@"name"];
//            g_delegate.curUser.mail = [jsonData valueForKey:@"mail"];
//            g_delegate.curUser.sessionID = [jsonData valueForKey:@"sessid"];
//            g_delegate.curUser.sessionName = [jsonData valueForKey:@"session_name"];
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
