//
//  SignInViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController <NSURLConnectionDelegate>
{
    IBOutlet UIButton* btnSignUp;
    
    IBOutlet UITextField* txtUserName;
    IBOutlet UITextField* txtPassword;
    
    NSURLConnection* urlConnection;
    NSMutableData* receivedData;
}

- (IBAction)onSignUp:(id)sender;
- (IBAction)onTextInputDone:(id)sender;
- (IBAction)onOK:(id)sender;
- (IBAction)onCancel:(id)sender;

@end
