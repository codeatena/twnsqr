//
//  GroupsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/17/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "GroupsViewController.h"
#import "Group.h"
#import "DataInf1TableViewCell.h"

@interface GroupsViewController ()

@end

@implementation GroupsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Group* group = nil;
    
    group = [[Group alloc] init];
    [arrData addObject:group];
    group = [[Group alloc] init];
    [arrData addObject:group];
    group = [[Group alloc] init];
    [arrData addObject:group];
    group = [[Group alloc] init];
    [arrData addObject:group];
    group = [[Group alloc] init];
    [arrData addObject:group];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    //    NSLog(@"%d", [arrData count]);
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"group_cell";
    
    DataInf1TableViewCell *cell = (DataInf1TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    DataInf1* dataInf1 = [arrData objectAtIndex:indexPath.row];
    
    [cell setDataInf1:dataInf1];
    
    return cell;
}

@end
