//
//  SetLocationViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "SetLocationViewController.h"
#import "define.h"

@interface SetLocationViewController ()

@end

@implementation SetLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onOK:(id)sender
{
    [txtAddress resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
}

- (IBAction)onCancel:(id)sender
{
    [txtAddress resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
}

- (IBAction)onTextInputDone:(id)sender
{
    UITextField* txtField = (UITextField*) sender;
    [txtField resignFirstResponder];
}

@end
