//
//  LocalEmergencyInfoViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "LocalEmergencyInfoViewController.h"
#import "LocalEmergencyInfo.h"
#import "SubLocalEmergencyInfo.h"
#import "define.h"

@interface LocalEmergencyInfoViewController ()

@end

@implementation LocalEmergencyInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadLocalEmergencyInfo];
    [self drawData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadLocalEmergencyInfo
{
    arrData = [[NSMutableArray alloc] init];

    SubLocalEmergencyInfo* subLocalEmergencyInfo = nil;
    NSMutableArray* arrText = nil;
    LocalEmergencyInfo* localEmergencyInfo = nil;
    
    localEmergencyInfo = [[LocalEmergencyInfo alloc] initWithTitle:@"EMERGENCY NUMBER: 911"];
    
    subLocalEmergencyInfo = [[SubLocalEmergencyInfo alloc] initWithTitle:@"Wood County Accident Report"];
    arrText = [[NSMutableArray alloc] initWithObjects:@"328 2nd Street", @"Parkersburg, WV 26101", @"(304) 424-1923", @"woodcountywvdeputies.com" , nil];
    subLocalEmergencyInfo.arrText = arrText;
    [localEmergencyInfo.arrSubLocalEmergencyInfo addObject:subLocalEmergencyInfo];
    
    subLocalEmergencyInfo = [[SubLocalEmergencyInfo alloc] initWithTitle:@"Parkersburg Fire Department"];
    arrText = [[NSMutableArray alloc] initWithObjects:@"1 Government Sq", @"Parkersburg, WV 26101", @"City of Parkersburg", @"(304) 424-8470", nil];
    subLocalEmergencyInfo.arrText = arrText;
    [localEmergencyInfo.arrSubLocalEmergencyInfo addObject:subLocalEmergencyInfo];
    
    [arrData addObject:localEmergencyInfo];
    
    localEmergencyInfo = [[LocalEmergencyInfo alloc] initWithTitle:@"HOSPITALS"];
    
    subLocalEmergencyInfo = [[SubLocalEmergencyInfo alloc] initWithTitle:@"Camden Clark Medical Center"];
    arrText = [[NSMutableArray alloc] initWithObjects:@"1824 Murdoch Ave", @"Parkersburg, WV 26101", @"(304) 424-4111", @"camdenclark.org" , nil];
    subLocalEmergencyInfo.arrText = arrText;
    [localEmergencyInfo.arrSubLocalEmergencyInfo addObject:subLocalEmergencyInfo];
    
    [arrData addObject:localEmergencyInfo];
}

- (void) drawData
{
    int maxY = 5;
    for (LocalEmergencyInfo* localEmergencyInfo in arrData) {
        maxY += 20;
        UILabel* lblLocalEmergencyInfoTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, maxY, 300, 20)];
//        lblLocalEmergencyInfoTitle.text = localEmergencyInfo.title;
        lblLocalEmergencyInfoTitle.textColor = UIColorFromRGB(0xe67904);
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:localEmergencyInfo.title];
        [attString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[localEmergencyInfo.title length]}];
        lblLocalEmergencyInfoTitle.attributedText = attString;
        maxY += 20;
        
        for (SubLocalEmergencyInfo* subLocalEmergencyInfo in localEmergencyInfo.arrSubLocalEmergencyInfo) {
            maxY += 20;
            UILabel* lblSubLocalEmergencyInfoTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, maxY, 300, 20)];
            lblSubLocalEmergencyInfoTitle.text = subLocalEmergencyInfo.title;
            lblSubLocalEmergencyInfoTitle.textColor = [UIColor blackColor];
            lblSubLocalEmergencyInfoTitle.font = [UIFont boldSystemFontOfSize:16];

            [scrollView addSubview:lblSubLocalEmergencyInfoTitle];
            maxY += 20;
            for (NSString* str in subLocalEmergencyInfo.arrText) {
                UILabel* lblText = [[UILabel alloc] initWithFrame:CGRectMake(10, maxY, 300, 20)];
                lblText.text = str;
                [scrollView addSubview:lblText];
                maxY += 20;
            }
        }
        [scrollView addSubview:lblLocalEmergencyInfoTitle];
        
    }
    
    [scrollView setContentSize:CGSizeMake(320, maxY)];
}

@end
