//
//  ExpandYourAreaViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpandYourAreaViewController : UIViewController
{
    IBOutlet UILabel* lblCurRadius;
}

- (IBAction)onMile1:(id)sender;
- (IBAction)onMile2:(id)sender;
- (IBAction)onMile5:(id)sender;
- (IBAction)onMile10:(id)sender;
- (IBAction)onMile25:(id)sender;
- (IBAction)onMile50:(id)sender;

@end
