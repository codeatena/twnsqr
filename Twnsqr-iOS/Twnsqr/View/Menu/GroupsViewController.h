//
//  GroupsViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/17/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZSwipeCell.h"

@interface GroupsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrData;
}

@end