//
//  ExpandYourAreaViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "ExpandYourAreaViewController.h"
#import "define.h"
#import "AppDelegate.h"

@interface ExpandYourAreaViewController ()

@end

@implementation ExpandYourAreaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) refreshButton:(id) sender;
{
    UIButton* btn = (UIButton*) sender;
    btn.backgroundColor = [UIColor whiteColor];
}

- (void) onMile: (id) sender WITH_RADIUS:(int) nRadius;
{
    UIButton* btn = (UIButton*) sender;
    btn.backgroundColor = [UIColor grayColor];
    g_delegate.nAreaRadius = nRadius;
    
    if  (nRadius == 1) {
        lblCurRadius.text = [NSString stringWithFormat:@"Your current area is %d mile from your location", nRadius];
    } else {
        lblCurRadius.text = [NSString stringWithFormat:@"Your current area is %d miles from your location", nRadius];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
    [self performSelector:@selector(refreshButton:) withObject:btn afterDelay:NAVIGATION_DURATION];
}

- (IBAction)onMile1:(id)sender
{
    [self onMile:sender WITH_RADIUS:1];
}

- (IBAction)onMile2:(id)sender
{
    [self onMile:sender WITH_RADIUS:2];
}

- (IBAction)onMile5:(id)sender
{
    [self onMile:sender WITH_RADIUS:5];
}

- (IBAction)onMile10:(id)sender
{
    [self onMile:sender WITH_RADIUS:10];
}

- (IBAction)onMile25:(id)sender
{
    [self onMile:sender WITH_RADIUS:25];
}

- (IBAction)onMile50:(id)sender
{
    [self onMile:sender WITH_RADIUS:50];
}

@end
