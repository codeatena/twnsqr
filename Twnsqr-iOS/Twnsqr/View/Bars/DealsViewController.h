//
//  DealsViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZSwipeCell.h"

@interface DealsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, JZSwipeCellDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrData;
}

@end