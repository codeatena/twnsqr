//
//  AlertsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "AlertsViewController.h"
#import "Alert.h"
#import "DataInf1TableViewCell.h"
#import "define.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"

@interface AlertsViewController ()

@end

@implementation AlertsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Alert* alert = nil;
    
    alert = [[Alert alloc] init];
    [arrData addObject:alert];
    alert = [[Alert alloc] init];
    [arrData addObject:alert];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
//    NSLog(@"%d", [arrData count]);
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"alert_cell";
    
    DataInf1TableViewCell *cell = (DataInf1TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    DataInf1* dataInf1 = [arrData objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    
    [cell setDataInf1:dataInf1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Alert* data = [arrData objectAtIndex:indexPath.row];
    
    Post* post = [self getPostFromData:data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_POST object:post];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)swipeCell:(JZSwipeCell*)cell triggeredSwipeWithType:(JZSwipeType)swipeType
{
	if (swipeType == JZSwipeTypeLongRight || swipeType == JZSwipeTypeShortRight)
	{
		NSIndexPath *indexPath = [myTableView indexPathForCell:cell];
		if (indexPath)
		{
			[arrData removeObjectAtIndex:indexPath.row];
			[myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
    if (swipeType == JZSwipeTypeShortLeft || swipeType == JZSwipeTypeLongLeft) {
        [myTableView reloadData];
    }
    //	[myTableView reloadData];
}

- (Post*) getPostFromData: (Alert*) data
{
    Post* post = [[Post alloc] init];
    post.title = @"Trash pickup has been moved one day later."; /// = data.title;
    post.header1 = data.header1;
    post.header2 = data.header2;
    
    Section* section = nil;
    PostData* postData = nil;
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = nil;
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"For the areas between the corner of 1st avenue and 16th street. Trash pickup will not be on Monday the 14th, Instead it is being moved to one day later Tuesday the 15th.";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"INFO";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"from the corner of 1st Avenue and 16 th stret to the Corner of 5 street and Percy Avenue, Trash will be delayed for one day.\nThis is due to the Momorial holiday.\nNormal Trash pickup will resume next week.";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"LOCATION:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"North Parkersburg Area";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"ALERT ACTIVE UNTIL:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"2:00 PM on Tue. Sep. 10th";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    return post;
}

@end
