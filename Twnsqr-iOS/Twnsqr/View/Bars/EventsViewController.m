//
//  EventsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "EventsViewController.h"
#import "DataInf2TableViewCell.h"
#import "define.h"
#import "Event.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"

@interface EventsViewController ()

@end

@implementation EventsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Event* event = nil;
    
    event = [[Event alloc] init];
    [arrData addObject:event];
    event = [[Event alloc] init];
    [arrData addObject:event];
    event = [[Event alloc] init];
    [arrData addObject:event];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrData count];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    //    NSLog(@"%d", [arrData count]);
    
    SectionDataInf2* sectionDataInf2 = [arrData objectAtIndex:section];
    
    return [sectionDataInf2.arrDataInf2 count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"event_cell";
    
    DataInf2TableViewCell *cell = (DataInf2TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    
    SectionDataInf2* sectionDataInf2 = [arrData objectAtIndex:indexPath.section];
    DataInf2* dataInf2 = [sectionDataInf2.arrDataInf2 objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    
    [cell setDataInf2:dataInf2];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 21)];
    view.backgroundColor = UIColorFromRGB(0xd1cfd2);
    
    SectionDataInf2* sectionDataInf2 = [arrData objectAtIndex:section];

    UILabel* lblHeader1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 21)];
    

    NSAttributedString *attString1 = [[NSAttributedString alloc]
                                            initWithString: sectionDataInf2.header1
                                            attributes: @{NSFontAttributeName: [UIFont italicSystemFontOfSize:
                                                                                15.0f]}];

    lblHeader1.attributedText = attString1;
    lblHeader1.textColor = UIColorFromRGB(0x828081);
    [view addSubview:lblHeader1];

    UILabel* lblHeader2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 21)];
    NSAttributedString *attString2 = [[NSAttributedString alloc]
                                      initWithString: sectionDataInf2.header2
                                      attributes: @{NSFontAttributeName: [UIFont italicSystemFontOfSize:
                                                                          15.0f]}];
    
    lblHeader2.attributedText = attString2;
    lblHeader2.textColor = UIColorFromRGB(0x828081);
    [lblHeader2 setTextAlignment:NSTextAlignmentRight];
    [view addSubview:lblHeader2];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Event* data = [arrData objectAtIndex:indexPath.row];
    
    Post* post = [self getPostFromData:data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_POST object:post];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)swipeCell:(JZSwipeCell*)cell triggeredSwipeWithType:(JZSwipeType)swipeType
{
	if (swipeType == JZSwipeTypeLongRight || swipeType == JZSwipeTypeShortRight)
	{
		NSIndexPath *indexPath = [myTableView indexPathForCell:cell];
		if (indexPath)
		{
            SectionDataInf2* sectionDataInf2 = [arrData objectAtIndex:indexPath.section];
//            DataInf2* dataInf2 = [sectionDataInf2.arrDataInf2 objectAtIndex:indexPath.row];
			[sectionDataInf2.arrDataInf2 removeObjectAtIndex:indexPath.row];
			[myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            if (sectionDataInf2.arrDataInf2.count == 0) {
                [arrData removeObjectAtIndex:indexPath.section];
                [myTableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
		}
	}
    if (swipeType == JZSwipeTypeShortLeft || swipeType == JZSwipeTypeLongLeft) {
        [myTableView reloadData];
    }
    //	[myTableView reloadData];
}

- (Post*) getPostFromData: (Event*) data
{
    Post* post = [[Post alloc] init];
    post.title = @"Art Show at The Gallery";
    post.header1 = data.header1;
    post.header2 = data.header2;
    
    Section* section = nil;
    PostData* postData = nil;
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"WHEN";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"3:00pm - 5:00pm";
    postData.nType = ITALIC_TYPE;
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"WHERE";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Parkersbur Art Gallery";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"601 Market St Parkersburg, WV";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"WHAT:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"The first ever Bill Belany Art show is now of display at the Parkersburg Art Gallery. Don't miss this one day event";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"COST:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"$3 Admission";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"WHO:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Parkersburg Art Gallery\nwww.parkersburgartgallery.com\n304-422-1515";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    return post;
}


@end
