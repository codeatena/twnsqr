//
//  AnnouncementsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "AnnouncementsViewController.h"
#import "Announcement.h"
#import "DataInf1TableViewCell.h"
#import "define.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"
#import "AppDelegate.h"

@interface AnnouncementsViewController ()

@end

@implementation AnnouncementsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
//    myTableView.editing = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Announcement* announcement = nil;
    
    announcement = [[Announcement alloc] init];
    [arrData addObject:announcement];
    announcement = [[Announcement alloc] init];
    [arrData addObject:announcement];
    announcement = [[Announcement alloc] init];
    [arrData addObject:announcement];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
//    NSLog(@"%d", [arrData count]);
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"announcement_cell";
    
    DataInf1TableViewCell *cell = (DataInf1TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    
    cell.delegate = self;
    
    DataInf1* dataInf1 = [arrData objectAtIndex:indexPath.row];
    
    [cell setDataInf1:dataInf1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Announcement* data = [arrData objectAtIndex:indexPath.row];
    
    Post* post = [self getPostFromData:data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_POST object:post];

    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)swipeCell:(JZSwipeCell*)cell triggeredSwipeWithType:(JZSwipeType)swipeType
{
	if (swipeType == JZSwipeTypeLongRight || swipeType == JZSwipeTypeShortRight)
	{
		NSIndexPath *indexPath = [myTableView indexPathForCell:cell];
		if (indexPath)
		{
			[arrData removeObjectAtIndex:indexPath.row];
			[myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
    if (swipeType == JZSwipeTypeShortLeft || swipeType == JZSwipeTypeLongLeft) {
        [myTableView reloadData];
    }
//	[myTableView reloadData];
}

- (Post*) getPostFromData: (Announcement*) data
{
    Post* post = [[Post alloc] init];
    post.title = @"Parkersburg Farmers Marketplace Downtown Parkersburg";
    post.header1 = data.header1;
    post.header2 = data.header2;
    
    Section* section = nil;
    PostData* postData = nil;

    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = nil;
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Dont't miss Farmers Marketplace Downtown Friday at the Bicentennial Plaza 302 Market St Parkersbur. WV";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"http://www/parlersburgfarmersmarket.com";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0xd76a00);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"INFO";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"304 422-5650";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x112d90);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"http://www/[arlersburgfarmersmarket.com";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0xd76a00);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Sponsored By: Area Roundtable";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"LOCATION:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Bicentenial Plaza";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"302 market StParkersburg. WV";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********

    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&

    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"HOURS:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"10:00 AM - 2:00 PM";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"COST:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"FREE";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    return post;
}

@end
