//
//  AttractionsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "AttractionsViewController.h"
#import "Attraction.h"
#import "DataInf2TableViewCell.h"
#import "define.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"

@interface AttractionsViewController ()

@end

@implementation AttractionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Attraction* attraction = nil;
    
    attraction = [[Attraction alloc] init];
    attraction.title = @"0.05miles Blenerhasset Island";
    [arrData addObject:attraction];
    attraction = [[Attraction alloc] init];
    attraction.title = @"0.75miles Oil and Gas Museum";
    [arrData addObject:attraction];
    attraction = [[Attraction alloc] init];
    attraction.title = @"1.20miles Glass Museum";
    [arrData addObject:attraction];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    //    NSLog(@"%d", [arrData count]);
    
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"attraction_cell";
    
    DataInf2TableViewCell *cell = (DataInf2TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    
    DataInf2* dataInf2 = [arrData objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    [cell setDataInf2:dataInf2];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 21)];
    view.backgroundColor = UIColorFromRGB(0xd1cfd2);
    
//    SectionDataInf2* sectionDataInf2 = [arrData objectAtIndex:section];
    
    UILabel* lblHeader1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 21)];
    
    
    NSAttributedString *attString1 = [[NSAttributedString alloc]
                                      initWithString:@"Distance From You"
                                      attributes: @{NSFontAttributeName: [UIFont italicSystemFontOfSize:
                                                                          15.0f]}];
    
    lblHeader1.attributedText = attString1;
    lblHeader1.textColor = UIColorFromRGB(0x828081);
    [view addSubview:lblHeader1];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Attraction* data = [arrData objectAtIndex:indexPath.row];
    
    Post* post = [self getPostFromData:data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_POST object:post];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)swipeCell:(JZSwipeCell*)cell triggeredSwipeWithType:(JZSwipeType)swipeType
{
	if (swipeType == JZSwipeTypeLongRight || swipeType == JZSwipeTypeShortRight)
	{
		NSIndexPath *indexPath = [myTableview indexPathForCell:cell];
		if (indexPath)
		{
			[arrData removeObjectAtIndex:indexPath.row];
			[myTableview deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
    if (swipeType == JZSwipeTypeShortLeft || swipeType == JZSwipeTypeLongLeft) {
        [myTableview reloadData];
    }
    //	[myTableView reloadData];
}

- (Post*) getPostFromData: (Attraction*) data
{
    Post* post = [[Post alloc] init];
    post.title = @"Blennerhasett Island Historical State Park";
    post.header1 = data.header1;
    post.header2 = data.header2;
    
    Section* section = nil;
    PostData* postData = nil;
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"WHERE";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"0.05 Miles From You";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"137 Juliana St";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Parkersburg, WV";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"(Access to island via sternwheeler";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"DETAILS:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"Site of the alleged plot by Aaron Burr and Harman Blennerhassett to establish a southwest empire. Features the reconstructed Blennerhassett Mansion, guided tours, concession stand, gift shop, narrated wagon rides, hiking trails and picnic facilities. Frequent special events include Mansion by Candlelight each October. Debarkation to the Island is from Point Park at the end of Second St in Parkersburg.";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"COST:";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"$3 Admission";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    return post;
}

@end
