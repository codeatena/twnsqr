//
//  DealsViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/11/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DealsViewController.h"
#import "Deal.h"
#import "DealTableViewCell.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"
#import "define.h"

@interface DealsViewController ()

@end

@implementation DealsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrData = [[NSMutableArray alloc] init];
    [self loadData];
    [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(calcRemaintime) userInfo:nil repeats:YES];
}

- (void) calcRemaintime
{
//    NSLog(@"calcRemaintime");
    [myTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadData
{
    Deal* deal = nil;
    deal = [[Deal alloc] init];
    [arrData addObject:deal];
    deal = [[Deal alloc] init];
    [arrData addObject:deal];
    deal = [[Deal alloc] init];
    [arrData addObject:deal];
    deal = [[Deal alloc] init];
    [arrData addObject:deal];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    //    NSLog(@"%d", [arrData count]);
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"deal_cell";
    
    DealTableViewCell *cell = (DealTableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    Deal* deal = [arrData objectAtIndex:indexPath.row];
    
    cell.delegate = self;
    
    [cell setDeal:deal];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Deal* data = [arrData objectAtIndex:indexPath.row];
    
    Post* post = [self getPostFromData:data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_POST object:post];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)swipeCell:(JZSwipeCell*)cell triggeredSwipeWithType:(JZSwipeType)swipeType
{
	if (swipeType == JZSwipeTypeLongRight || swipeType == JZSwipeTypeShortRight)
	{
		NSIndexPath *indexPath = [myTableView indexPathForCell:cell];
		if (indexPath)
		{
			[arrData removeObjectAtIndex:indexPath.row];
			[myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
    if (swipeType == JZSwipeTypeShortLeft || swipeType == JZSwipeTypeLongLeft) {
        [myTableView reloadData];
    }
    //	[myTableView reloadData];
}

- (Post*) getPostFromData: (Deal*) data
{
    Post* post = [[Post alloc] init];
    post.title = @"$2 Double Cheeseburger DEAL";
    post.header1 = data.header;
    post.header2 = @"";
    post.date = data.date;
    
    Section* section = nil;
    PostData* postData = nil;
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = nil;
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"For the next 30 Minutes get any double cheesburger for only $2";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = nil;
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"While supllies last, deal exlude Super Bacon Double cheeseburgers. Only 2 per customer.";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"Map it";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0x2203cb);
    postData.content = @"121Broad Street Parkersburg WV";
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"Call Them";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.nType = UNDERLINE_TYPE;
    postData.content = @"304-485-1212";
    postData.color = UIColorFromRGB(0x277800);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD PART START &&&&&&&
    section = [[Section alloc] init];
    section.header = @"Visit the Website";
    
    //*******************  POST ADD PART START **********
    postData = [[PostData alloc] init];
    postData.content = @"www.bobsburgerswv.com";
    postData.nType = UNDERLINE_TYPE;
    postData.color = UIColorFromRGB(0xca6606);
    [section.arrPostData addObject:postData];
    //*******************  POST ADD PART  END  **********
    
    [post.arrSection addObject:section];
    //&&&&&&&&&&&&&&&&&&&  SECTION ADD APRT END &&&&&&&&&
    
    return post;
}

@end
