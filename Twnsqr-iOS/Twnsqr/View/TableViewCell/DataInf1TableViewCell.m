//
//  DataInf1TableViewCell.m
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DataInf1TableViewCell.h"
#import "define.h"

@implementation DataInf1TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
	self.imageSet = SwipeCellImageSetMake([UIImage imageNamed:@"cell_remove.png"], [UIImage imageNamed:@"cell_remove.png"], [UIImage imageNamed:@"cell_save.png"], [UIImage imageNamed:@"cell_save.png"]);
}

- (void)prepareForReuse
{
	[super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onSave:(id)sender
{
    
}

- (IBAction)onSwipe:(id)sender
{
    
}

- (IBAction)onRemove:(id)sender
{
    
}

- (void) setDataInf1: (DataInf1*) _dataInf1;
{
    dataInf1 = _dataInf1;
    lblHeader1.text = dataInf1.header1;
    lblHeader2.text = dataInf1.header2;
    lblTitle.text = dataInf1.title;
    lblDesc.text = dataInf1.desc;
    
    lblHeaderBack.backgroundColor = UIColorFromRGB(0xd1cfd2);
    lblHeader1.textColor = UIColorFromRGB(0x828081);
    lblHeader2.textColor = UIColorFromRGB(0x828081);
    
    [btnSave setTitleColor:UIColorFromRGB(0x767475) forState:UIControlStateNormal];
    [btnRemove setTitleColor: UIColorFromRGB(0x767475) forState:UIControlStateNormal];
    [btnSwipe setTitleColor: UIColorFromRGB(0xe8973c) forState:UIControlStateNormal];
}

@end
