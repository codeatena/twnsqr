//
//  DataInf1TableViewCell.h
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataInf1.h"
#import "JZSwipeCell.h"

@interface DataInf1TableViewCell : JZSwipeCell
{
    IBOutlet UILabel* lblHeader1;
    IBOutlet UILabel* lblHeader2;
    
    IBOutlet UILabel* lblTitle;
    IBOutlet UILabel* lblDesc;
    
    IBOutlet UIButton* btnSave;
    IBOutlet UIButton* btnSwipe;
    IBOutlet UIButton* btnRemove;
    
    IBOutlet UILabel* lblHeaderBack;
    
    DataInf1* dataInf1;
}

- (IBAction)onSave:(id)sender;
- (IBAction)onSwipe:(id)sender;
- (IBAction)onRemove:(id)sender;

- (void) setDataInf1: (DataInf1*) _dataInf1;

@end
