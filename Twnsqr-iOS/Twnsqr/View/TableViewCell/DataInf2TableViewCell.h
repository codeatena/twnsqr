//
//  DataInf2TableViewCell.h
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataInf2.h"
#import "JZSwipeCell.h"

@interface DataInf2TableViewCell : JZSwipeCell
{
    IBOutlet UILabel* lblTitle;
    IBOutlet UIButton* btnSave;
    IBOutlet UIButton* btnSwipe;
    IBOutlet UIButton* btnRemove;
    
    DataInf2* dataInf2;
}

- (IBAction)onSave:(id)sender;
- (IBAction)onSwipe:(id)sender;
- (IBAction)onRemove:(id)sender;

- (void) setDataInf2:(DataInf2*) _dataInf2;

@end
