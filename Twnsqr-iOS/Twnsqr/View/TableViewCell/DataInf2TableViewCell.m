//
//  DataInf2TableViewCell.m
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "DataInf2TableViewCell.h"
#import "define.h"

@implementation DataInf2TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
	self.imageSet = SwipeCellImageSetMake([UIImage imageNamed:@"cell_remove.png"], [UIImage imageNamed:@"cell_remove.png"], [UIImage imageNamed:@"cell_save.png"], [UIImage imageNamed:@"cell_save.png"]);
}

- (void)prepareForReuse
{
	[super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onSave:(id)sender
{
    
}

- (IBAction)onSwipe:(id)sender
{
    
}

- (IBAction)onRemove:(id)sender
{
    
}

- (void) setDataInf2:(DataInf2*) _dataInf2
{
    dataInf2 = _dataInf2;
    lblTitle.text = _dataInf2.title;
    
    [btnSave setTitleColor:UIColorFromRGB(0x767475) forState:UIControlStateNormal];
    [btnRemove setTitleColor: UIColorFromRGB(0x767475) forState:UIControlStateNormal];
    [btnSwipe setTitleColor: UIColorFromRGB(0xe8973c) forState:UIControlStateNormal];
}

@end
