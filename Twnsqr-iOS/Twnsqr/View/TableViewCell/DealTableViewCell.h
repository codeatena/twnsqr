//
//  DealTableViewCell.h
//  Twnsqr
//
//  Created by Hang Chung on 6/13/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deal.h"
#import "JZSwipeCell.h"

@interface DealTableViewCell : JZSwipeCell
{
    IBOutlet UILabel* lblHeaderBack;
    IBOutlet UILabel* lblHeader;
    IBOutlet UILabel* lblTitle;
    IBOutlet UILabel* lblTime;
    IBOutlet UILabel* lblDesc;
    IBOutlet UILabel* lblLocation;
    IBOutlet UILabel* lblSite;
    
    IBOutlet UIButton* btnSave;
    IBOutlet UIButton* btnSwipe;
    IBOutlet UIButton* btnRemove;
    
    Deal* deal;
}

- (IBAction)onSave:(id)sender;
- (IBAction)onSwipe:(id)sender;
- (IBAction)onRemove:(id)sender;

- (void) setDeal: (Deal*) _deal;

@end