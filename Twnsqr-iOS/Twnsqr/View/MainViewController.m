//
//  MainViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/10/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "MainViewController.h"
#import "define.h"
#import "KxMenu.h"
#import "ReportViewController.h"
#import "AppDelegate.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [scrollButton setContentSize:CGSizeMake(620, 30)];
    
    reportViewCtrl = [[ReportViewController alloc] initWithNibName:@"ReportViewController" bundle:nil];

    btnHome.hidden = YES;
    nBarIndex = ANNOUNCEMENTES;
    nMenuIndex = MENU_NONE;
    
    viewLocal.hidden = YES;
    viewGroup.hidden = YES;
    viewGroups.hidden = YES;
    viewLocation.hidden = YES;
    viewExpand.hidden = YES;
    viewProfile.hidden = YES;
    viewSettings.hidden = YES;
    viewSignIn.hidden = YES;
    viewSignUp.hidden = YES;
    
    [self initNotification];
    [self selBarType];
    [self initLocationManager];
}

- (void) initNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:NOTIFICATION_NAME_MENU_POP
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:NOTIFICATION_NAME_MENU_PUSH_SIGN_UP
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:NOTIFICATION_NAME_POST
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:NOTIFICATION_NAME_MENU_PUSH_GROUPS
                                               object:nil];
}

- (void) initLocationManager
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    g_delegate.curLat = [[NSString alloc]
                                 initWithFormat:@"%.6f",
                                 newLocation.coordinate.latitude];
    
    g_delegate.curLng = [[NSString alloc]
                                  initWithFormat:@"%.6f",
                                  newLocation.coordinate.longitude];
    
    NSLog(@"latitude = %@, longitude = %@", g_delegate.curLat, g_delegate.curLng);
}

- (void) receiveNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:NOTIFICATION_NAME_MENU_POP]) {
        [self popMenuViewAnimation];
    }
    if ([[notification name] isEqualToString:NOTIFICATION_NAME_MENU_PUSH_SIGN_UP]) {
        [self pushMenuView:SIGN_UP];
    }
    if ([[notification name] isEqualToString:NOTIFICATION_NAME_MENU_PUSH_GROUPS]) {
        [self pushMenuView:GROUPS];
    }
    if ([[notification name] isEqualToString:NOTIFICATION_NAME_POST]) {
//        NSLog(@"mainviewcontroller:%@", NOTIFICATION_NAME_POST);
        [self pushMenuView:POST];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onAnnouncements:(id)sender
{
    nBarIndex = ANNOUNCEMENTES;
    [self selBarType];
}

- (void) selAnnouncements
{
    imgBar.image = [UIImage imageNamed:@"bar_events.png"];
    lblBar.text = @"ANNOUNCEMENTS";
    lblBar.textColor = [UIColor blackColor];
    [btnAnnouncements setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewAnnoucements.hidden = NO;
}

- (IBAction)onAlerts:(id)sender;
{
    nBarIndex = ALERTS;
    [self selBarType];
}

- (void) selAlerts
{
    imgBar.image = [UIImage imageNamed:@"bar_alert.png"];
    lblBar.text = @"ALERTS";
    lblBar.textColor = [UIColor whiteColor];
    [btnAlerts setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewAlerts.hidden = NO;
}

- (IBAction)onEmergency:(id)sender
{
    nBarIndex = EMERGENCY;
    [self selBarType];
}

- (void) selEmergency
{
    imgBar.image = [UIImage imageNamed:@"bar_emergency.png"];
    lblBar.text = @"EMERGENCY";
    lblBar.textColor = [UIColor whiteColor];
    [btnEmergency setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewEmergency.hidden = NO;
}

- (IBAction)onDeals:(id)sender
{
    nBarIndex = DEALS;
    [self selBarType];
}

- (void) selDeals
{
    imgBar.image = [UIImage imageNamed:@"bar_deals.png"];
    lblBar.text = @"DEALS";
    lblBar.textColor = [UIColor whiteColor];
    [btnDeals setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewDeals.hidden = NO;
}

- (IBAction)onEvents:(id)sender
{
    nBarIndex = EVENTS;
    [self selBarType];
}

- (void) selEvents
{
    imgBar.image = [UIImage imageNamed:@"bar_events.png"];
    lblBar.text = @"EVENTS";
    lblBar.textColor = [UIColor blackColor];
    [btnEvents setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewEvents.hidden = NO;
}

- (IBAction)onAttractions:(id)sender
{
    nBarIndex = ATTRACTIONS;
    [self selBarType];
}

- (void) selAttractions
{
    imgBar.image = [UIImage imageNamed:@"bar_events.png"];
    lblBar.text = @"ATTRACTIONS";
    lblBar.textColor = [UIColor blackColor];
    [btnAttractions setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewAttractions.hidden = NO;
}

- (IBAction)onJobs:(id)sender
{
    nBarIndex = JOBS;
    [self selBarType];
}

- (void) selJobs
{
    imgBar.image = [UIImage imageNamed:@"bar_events.png"];
    lblBar.text = @"JOBS";
    lblBar.textColor = [UIColor blackColor];
    [btnJobs setTitleColor:UIColorFromRGB(0xeb930b) forState:UIControlStateNormal];
    viewJobs.hidden = NO;
}

- (IBAction)onPrev:(id)sender
{
    if (nBarIndex == ANNOUNCEMENTES) {
        return;
    }
    
    nBarIndex--;
    [self selBarType];
}

- (IBAction)onNext:(id)sender
{
    if (nBarIndex == JOBS) {
        return;
    }
    
    nBarIndex++;
    [self selBarType];
}

- (void) selBarType
{
    [self refreshAllBarButton];
    if (nBarIndex == ANNOUNCEMENTES) [self selAnnouncements];
    if (nBarIndex == ALERTS) [self selAlerts];
    if (nBarIndex == EMERGENCY) [self selEmergency];
    if (nBarIndex == DEALS) [self selDeals];
    if (nBarIndex == EVENTS) [self selEvents];
    if (nBarIndex == ATTRACTIONS) [self selAttractions];
    if (nBarIndex == JOBS) [self selJobs];
}

- (void) refreshAllBarButton
{
    [btnAnnouncements setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnAlerts setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEmergency setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDeals setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEvents setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnAttractions setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnJobs setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    viewAnnoucements.hidden = YES;
    viewAlerts.hidden = YES;
    viewEmergency.hidden = YES;
    viewDeals.hidden = YES;
    viewEvents.hidden = YES;
    viewAttractions.hidden = YES;
    viewJobs.hidden = YES;
    
    if (nMenuIndex > MENU_NONE)
        [self popMenuViewAnimation];
}

- (IBAction)onMenu:(UIButton*)sender
{
    NSArray *menuItems = nil;
    
    if (g_delegate.curUser == nil) {
        
        menuItems = @[
          
          [KxMenuItem menuItem:@"Local Emergency Info"
                         image:nil
                        target:self
                        action:@selector(menuLocalEmergencyInfo:)],
          
          [KxMenuItem menuItem:@"Set Location"
                         image:nil
                        target:self
                        action:@selector(menuSetLocation:)],
          
          [KxMenuItem menuItem:@"Find My Location"
                         image:nil
                        target:self
                        action:@selector(menuFindMyLocation:)],
          
          [KxMenuItem menuItem:@"Exapnd Your Area"
                         image:nil
                        target:self
                        action:@selector(menuExpandYourArea:)],
          
          [KxMenuItem menuItem:@"Settings"
                         image:nil
                        target:self
                        action:@selector(menuSettings:)],
          
          [KxMenuItem menuItem:@"Sign In"
                         image:nil
                        target:self
                        action:@selector(menuSignIn:)],
          
          [KxMenuItem menuItem:@"Report on error"
                         image:nil
                        target:self
                        action:@selector(menuReportOnError:)],
          ];
    } else {
        menuItems = @[
                      
                      [KxMenuItem menuItem:@"Local Emergency Info"
                                     image:nil
                                    target:self
                                    action:@selector(menuLocalEmergencyInfo:)],
                      
                      [KxMenuItem menuItem:@"Groups"
                                     image:nil
                                    target:self
                                    action:@selector(menuGroup:)],
                      
                      [KxMenuItem menuItem:@"Set Location"
                                     image:nil
                                    target:self
                                    action:@selector(menuSetLocation:)],
                      
                      [KxMenuItem menuItem:@"Find My Location"
                                     image:nil
                                    target:self
                                    action:@selector(menuFindMyLocation:)],
                      
                      [KxMenuItem menuItem:@"Exapnd Your Area"
                                     image:nil
                                    target:self
                                    action:@selector(menuExpandYourArea:)],
                      
                      [KxMenuItem menuItem:@"Profile"
                                     image:nil
                                    target:self
                                    action:@selector(menuProfile:)],
                      
                      [KxMenuItem menuItem:@"Settings"
                                     image:nil
                                    target:self
                                    action:@selector(menuSettings:)],
                      
                      [KxMenuItem menuItem:@"Sign Out"
                                     image:nil
                                    target:self
                                    action:@selector(menuSignOut:)],
                      
                      [KxMenuItem menuItem:@"Report on error"
                                     image:nil
                                    target:self
                                    action:@selector(menuReportOnError:)],
                      ];
    }
    

    
    //    KxMenuItem *first = menuItems[0];
    //    first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
    //    first.alignment = NSTextAlignmentCenter;
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

- (void) menuLocalEmergencyInfo:(id) sender
{
    [self pushMenuView:LOCAL_EMERGENCY_INFO];
}

- (void) menuGroup:(id) sender
{
    [self pushMenuView:GROUP];
}

- (void) menuSetLocation:(id) sender
{
    [self pushMenuView:SET_LOCATION];
}

- (void) menuFindMyLocation:(id) sender
{
    NSLog(@"Find My Location");
    NSLog(@"latitude = %@, longitude = %@, radius = %d", g_delegate.curLat, g_delegate.curLng, g_delegate.nAreaRadius);

    NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:
                                     [NSURL URLWithString: URL_FIND_LOCATION]
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    g_delegate.curLat, @"lat",
                                    g_delegate.curLng, @"lng",
                                    [NSString stringWithFormat:@"%d", g_delegate.nAreaRadius], @"radius",
                                    nil];
    
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    receivedDataFindLocation = [NSMutableData dataWithCapacity: 0];
    
    // should check for and handle errors here but we aren't
    [urlRequest setHTTPBody:jsonData];
    
    urlConnectionFindLocation = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

- (void) menuExpandYourArea:(id) sender
{
    [self pushMenuView:EXPAND_YOUR_AREA];
}

- (void) menuProfile:(id) sender
{
    
}

- (void) menuSettings:(id) sender
{
    [self pushMenuView:SETTINGS];
}

- (void) menuSignIn:(id) sender
{
    [self pushMenuView:SIGN_IN];
}

- (void) menuSignOut:(id) sender
{
//    self onAnnouncements:
    NSLog(@"Sign out");
    NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:
                                     [NSURL URLWithString: URL_LOGOUT]
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    g_delegate.curUser.sessionID, @"sessid",
                                    g_delegate.curUser.sessionName, @"sessname",
                                    nil];
    
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    receivedDataSignOut = [NSMutableData dataWithCapacity: 0];
    
    // should check for and handle errors here but we aren't
    [urlRequest setHTTPBody:jsonData];
    
    urlConnectionSignOut = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_MENU_POP object:nil];
}

- (void) menuReportOnError:(id) sender
{
//    ReportViewController* reportViewCtrl = [[UIStoryboard storyboardWithName:@"twnsqr_iphone" bundle:nil] instantiateViewControllerWithIdentifier:@"ReportViewController"];
//    [self.view addSubview:reportViewCtrl.view];
    [self.view addSubview:reportViewCtrl.view];
//    [self performSegueWithIdentifier:@"main_modal_report" sender:nil];
}

- (IBAction)onHome:(id)sender
{
    [self popMenuViewAnimation];
}

- (void) pushMenuView:(int) _nMenuIndex
{
    if (nMenuIndex == _nMenuIndex) {
        return;
    }
    if (nMenuIndex == MENU_NONE) {
        nMenuIndex = _nMenuIndex;
        [self pushMenuViewAnimation];
        return;
    }
    
    [self popMenuViewAnimation];
    nMenuIndex = _nMenuIndex;
    [self performSelector:@selector(pushMenuViewAnimation) withObject:nil afterDelay:NAVIGATION_DURATION];
}

- (void) pushMenuViewAnimation
{
    UIView* viewMenuPush = nil;
    
    if (nMenuIndex == LOCAL_EMERGENCY_INFO) viewMenuPush = viewLocal;
    if (nMenuIndex == GROUP) viewMenuPush = viewGroup;
    if (nMenuIndex == GROUPS) viewMenuPush = viewGroups;
    if (nMenuIndex == SET_LOCATION) viewMenuPush = viewLocation;
    if (nMenuIndex == EXPAND_YOUR_AREA) viewMenuPush = viewExpand;
    if (nMenuIndex == PROFILE) viewMenuPush = viewProfile;
    if (nMenuIndex == SETTINGS) viewMenuPush = viewSettings;
    if (nMenuIndex == SIGN_IN) viewMenuPush = viewSignIn;
    if (nMenuIndex == SIGN_UP) viewMenuPush = viewSignUp;
    if (nMenuIndex == POST) viewMenuPush = viewPost;
    
    viewMenuPush.hidden = NO;
    btnPrev.enabled = NO;
    btnNext.hidden = YES;
    btnHome.hidden = NO;
    
    CGRect curRect = viewMenuPush.frame;
    viewMenuPush.frame = CGRectMake(320, curRect.origin.y, curRect.size.width, curRect.size.height);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:NAVIGATION_DURATION];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    viewMenuPush.frame = CGRectMake(0, curRect.origin.y, curRect.size.width, curRect.size.height);
    
    [UIView commitAnimations];
}

- (void) popMenuViewAnimation
{
    UIView* viewMenuPop = nil;
    
    if (nMenuIndex == LOCAL_EMERGENCY_INFO) viewMenuPop = viewLocal;
    if (nMenuIndex == GROUP) viewMenuPop = viewGroup;
    if (nMenuIndex == GROUPS) viewMenuPop = viewGroups;
    if (nMenuIndex == SET_LOCATION) viewMenuPop = viewLocation;
    if (nMenuIndex == EXPAND_YOUR_AREA) viewMenuPop = viewExpand;
    if (nMenuIndex == PROFILE) viewMenuPop = viewProfile;
    if (nMenuIndex == SETTINGS) viewMenuPop = viewSettings;
    if (nMenuIndex == SIGN_IN) viewMenuPop = viewSignIn;
    if (nMenuIndex == SIGN_UP) viewMenuPop = viewSignUp;
    if (nMenuIndex == POST) viewMenuPop = viewPost;
    
    viewMenuPop.hidden = NO;
    btnPrev.enabled = YES;
    btnNext.hidden = NO;
    btnHome.hidden = YES;
    
    CGRect curRect = viewMenuPop.frame;
    viewMenuPop.frame = CGRectMake(0, curRect.origin.y, curRect.size.width, curRect.size.height);
    
    nMenuIndex = MENU_NONE;

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:NAVIGATION_DURATION];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    viewMenuPop.frame = CGRectMake(320, curRect.origin.y, curRect.size.width, curRect.size.height);
    
    [UIView commitAnimations];
    
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnectionSignOut) {
        [receivedDataSignOut appendData:data];
    }
    if (connection == urlConnectionFindLocation) {
        [receivedDataFindLocation appendData:data];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnectionSignOut) {
        // use data from data_users
        NSString *myString = [[NSString alloc] initWithData:receivedDataSignOut encoding:NSUTF8StringEncoding];
        
        NSLog(@"sign out result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedDataSignOut
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        NSString* strMsg = [jsonObject objectForKey:@"msg"];
        
        if ([strMsg isEqualToString:@"User is logged out."]) {
            g_delegate.curUser = nil;
        }
    }
    if (connection == urlConnectionFindLocation) {
        NSString *myString = [[NSString alloc] initWithData:receivedDataFindLocation encoding:NSUTF8StringEncoding];
        
        NSLog(@"find location result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedDataFindLocation
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        id jsonData = [jsonObject objectForKey:@"data"];
        
        lblAddress.text = [jsonData objectForKey:@"address"];
        NSString* str = [jsonData objectForKey:@"logo"];
                
        NSURL *url = [NSURL URLWithString:str];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        imgLogo.image = image;
    }
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    connection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
