//
//  MainViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/10/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface MainViewController : UIViewController <NSURLConnectionDelegate, CLLocationManagerDelegate>
{
    IBOutlet UIButton*  btnAnnouncements;
    IBOutlet UIButton*  btnAlerts;
    IBOutlet UIButton*  btnEmergency;
    IBOutlet UIButton*  btnDeals;
    IBOutlet UIButton*  btnEvents;
    IBOutlet UIButton*  btnAttractions;
    IBOutlet UIButton*  btnJobs;
    
    IBOutlet UIScrollView*  scrollButton;
    IBOutlet UIImageView*   imgBar;
    IBOutlet UILabel* lblBar;
    
    IBOutlet UIButton*  btnHome;
    
    IBOutlet UIButton*  btnPrev;
    IBOutlet UIButton*  btnNext;
    
    IBOutlet UIView*    viewAnnoucements;
    IBOutlet UIView*    viewAlerts;
    IBOutlet UIView*    viewEmergency;
    IBOutlet UIView*    viewDeals;
    IBOutlet UIView*    viewEvents;
    IBOutlet UIView*    viewAttractions;
    IBOutlet UIView*    viewJobs;
    
    IBOutlet UIView*    viewLocal;
    IBOutlet UIView*    viewGroup;
    IBOutlet UIView*    viewGroups;
    IBOutlet UIView*    viewLocation;
    IBOutlet UIView*    viewExpand;
    IBOutlet UIView*    viewSettings;
    IBOutlet UIView*    viewSignIn;
    IBOutlet UIView*    viewProfile;
    IBOutlet UIView*    viewSignUp;
    IBOutlet UIView*    viewPost;
    
    IBOutlet UILabel*   lblAddress;
    IBOutlet UIImageView*   imgLogo;
    
    int nBarIndex;
    int nMenuIndex;
    
    ReportViewController* reportViewCtrl;
    
    NSURLConnection* urlConnectionSignOut;
    NSURLConnection* urlConnectionFindLocation;
    
    NSMutableData* receivedDataSignOut;
    NSMutableData* receivedDataFindLocation;
    
    CLLocationManager *locationManager;
}

- (IBAction)onAnnouncements:(id)sender;
- (IBAction)onAlerts:(id)sender;
- (IBAction)onEmergency:(id)sender;
- (IBAction)onDeals:(id)sender;
- (IBAction)onAttractions:(id)sender;
- (IBAction)onJobs:(id)sender;
- (IBAction)onEvents:(id)sender;

- (IBAction)onMenu:(id)sender;
- (IBAction)onPrev:(id)sender;
- (IBAction)onNext:(id)sender;

- (IBAction)onHome:(id)sender;

@end
