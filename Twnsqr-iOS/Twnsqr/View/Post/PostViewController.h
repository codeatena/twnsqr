//
//  PostViewController.h
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface PostViewController : UIViewController
{
    IBOutlet UIScrollView* scrollView;
    
    IBOutlet UILabel*   lblHeaderBack;
    IBOutlet UILabel*   lblHeader1;
    IBOutlet UILabel*   lblHeader2;
    
    UILabel*            lblTimer;
    NSTimer*            timer;
}

@property (nonatomic, retain) Post* post;

@end
