//
//  PostViewController.m
//  Twnsqr
//
//  Created by Hang Chung on 6/14/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "PostViewController.h"
#import "define.h"
#import "Post.h"
#import "Section.h"
#import "PostData.h"
#import "Deal.h"

@interface PostViewController ()

@end

@implementation PostViewController

@synthesize post;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initNotification];
    
    lblHeaderBack.backgroundColor = UIColorFromRGB(0xd1cfd2);
    lblHeader1.textColor = UIColorFromRGB(0x828081);
    lblHeader2.textColor = UIColorFromRGB(0x828081);
    
    lblTimer = [[UILabel alloc] initWithFrame:CGRectMake(225, 2, 95, 40)];
    lblTimer.backgroundColor = [UIColor blackColor];
    lblTimer.textColor = UIColorFromRGB(0xe50900);
    lblTimer.textAlignment = NSTextAlignmentCenter;
    lblTimer.font = [UIFont boldSystemFontOfSize:25.0f];
    [NSTimer scheduledTimerWithTimeInterval:.5f target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
//    lblTimer.hidden = NO;

//    lblTimer.layer.zPosition = 0;
    
//    [self.view addSubview:lblTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:NOTIFICATION_NAME_POST
                                               object:nil];
}

- (void) receiveNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.


    if ([[notification name] isEqualToString:NOTIFICATION_NAME_POST]) {
//        NSLog(@"popviewcontroller:%@", NOTIFICATION_NAME_POST);
        post = (Post*) notification.object;
        [self drawPost];
    }
}

- (void) drawPost
{
    lblHeader1.text = post.header1;
    lblHeader2.text = post.header2;
    
    for (UIView* view in [scrollView subviews]) {
        [view removeFromSuperview];
    }
    
    int h = 10;
    UILabel* lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, h, 300, 45)];
    lblTitle.text = post.title;
    lblTitle.numberOfLines = 10;
    
//    lblTitle.backgroundColor = [UIColor greenColor];
    lblTitle.attributedText = [[NSAttributedString alloc]
                              initWithString:post.title
                               attributes: @{NSFontAttributeName:[UIFont italicSystemFontOfSize:18]}];
    
    if (post.date != nil) {
        [scrollView addSubview:lblTimer];
        lblTitle.frame = CGRectMake(10, h, 205, 45);
    }
    
    [lblTitle sizeToFit];
    
//    NSLog(@"title height = %f", lblTitle.frame.size.height);
    [scrollView addSubview:lblTitle];
    h += lblTitle.frame.size.height;
    
    for (Section* section in post.arrSection) {
        if (section.header) {
            h += 20;
            
            UILabel* lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, h, 300, 20)];
//            lblHeader.backgroundColor = [UIColor greenColor];
            lblHeader.font = [UIFont boldSystemFontOfSize:15.0f];
            lblHeader.text = section.header;

            [scrollView addSubview:lblHeader];
            h += 20;
        } else {
            h += 10;
        }
        
        for (PostData* postData in section.arrPostData) {
            
            UILabel* lblPostData = [[UILabel alloc] initWithFrame:CGRectMake(10, h, 300, 20)];
            lblPostData.textColor = postData.color;
//            lblPostData.backgroundColor = [UIColor redColor];
            lblPostData.numberOfLines = 100;
            
            if (postData.nType == UNDERLINE_TYPE) {
                lblPostData.font = [UIFont systemFontOfSize:14];
                lblPostData.attributedText = [[NSAttributedString alloc]
                                           initWithString:postData.content
                                           attributes: @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
            } else if (postData.nType == ITALIC_TYPE) {
                lblPostData.attributedText = [[NSAttributedString alloc]
                                              initWithString:postData.content
                                              attributes: @{NSFontAttributeName:[UIFont italicSystemFontOfSize:14]}];
            } else {
                lblPostData.font = [UIFont systemFontOfSize:14];
                lblPostData.text = postData.content;
            }
            
            [lblPostData sizeToFit];

            [scrollView addSubview:lblPostData];
            h += lblPostData.frame.size.height;
        }
    }
    
    h += 15;
    
    UIButton* btnSave = [[UIButton alloc] initWithFrame:CGRectMake(20, h - 5, 40, 40)];
    UIButton* btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(260, h, 30, 30)];
    
    [btnSave setTitle:@"" forState:UIControlStateNormal];
    [btnDelete setTitle:@"" forState:UIControlStateNormal];
    [btnSave setBackgroundImage:[UIImage imageNamed:@"btn_save.png"] forState:UIControlStateNormal];
    [btnDelete setBackgroundImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
    
    [btnSave addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
    [btnDelete addTarget:self action:@selector(onDelete:) forControlEvents:UIControlEventTouchUpInside];

    [scrollView addSubview:btnSave];
    [scrollView addSubview:btnDelete];

    h += 40;
    [scrollView setContentSize:CGSizeMake(320, h)];
}

- (void) onTimer
{
    lblTimer.text = [Deal getRemainTime:post.date];
}

- (void) onSave:(id) sender
{
    NSLog(@"save");
}

- (void) onDelete:(id) sender
{
    NSLog(@"delete");
}

@end
