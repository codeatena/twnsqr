//
//  AppDelegate.h
//  Twnsqr
//
//  Created by Hang Chung on 6/9/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) int nAreaRadius;
@property (nonatomic, retain) User* curUser;

@property (nonatomic, retain) NSString* curLat;
@property (nonatomic, retain) NSString* curLng;

- (void)showWaitingScreen:(NSString *)strText bShowText:(BOOL)bShowText;
- (void)hideWaitingScreen;

@end

AppDelegate* g_delegate;